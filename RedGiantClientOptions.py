import sys
import os

RENDER_ONLY = "REDGIANT_RENDER_ONLY"
CLIENT_LOGGING = "REDGIANT_ENTERPRISE_LOGGING"

options = {
    RENDER_ONLY : False,
    CLIENT_LOGGING : False
}


# Saves the current settings to the rlm-options.txt file
def save_options():
    path = get_path()
    render = RENDER_ONLY + "=true\n" if options[RENDER_ONLY] else RENDER_ONLY + "=false\n"
    logging = CLIENT_LOGGING + "=true\n" if options[CLIENT_LOGGING] else CLIENT_LOGGING + "=false\n"
    lines = [render, logging]
    file = open(path, "w")
    file.writelines(lines)
    file.close()
    print("Saved!")


# Prints the status of the RLM options
def print_options():
    print("Render Only: ON") if options[RENDER_ONLY] else print("Render Only: OFF")
    print("Client Logging: ON") if options[CLIENT_LOGGING] else print("Client Logging: OFF")


# Gets the platform (OS) of the machine running the script
def get_platform():
    platforms = {
        "linux1" : "Linux",
        "linux2" : "Linux",
        "darwin" : "Mac OSX",
        "win32" : "Windows"
    }

    if sys.platform not in platforms:
        return sys.platform
    else:
        return platforms[sys.platform]


# Gets the OS-dependent path to the RLM options file
def get_path():
    paths = {
        "Windows": "C:\\ProgramData\\Red Giant\\licenses\\rlm-options.txt",
        "Mac OSX": "/Users/Shared/Red Giant/licenses/rlm-options.txt"
    }
    platform = get_platform()
    # print("Your platform: %s" % platform)
    if platform not in paths:
        Exception("Operating system not supported")
    else:
        return paths[platform]


# Reads the RLM options file and loads the current settings
def get_options():
    try:
        path = get_path()
        if os.path.exists(path):
            print("Reading current settings...")
            lines = []
            with open(path, "r") as file:
                data = file.read()
                lines = data.splitlines()
            for line in lines:
                if RENDER_ONLY in line:
                    if "true" in line:
                        options[RENDER_ONLY] = True
                    else:
                        options[RENDER_ONLY] = False
                if CLIENT_LOGGING in line:
                    if "true" in line:
                        options[CLIENT_LOGGING] = True
                    else:
                        options[CLIENT_LOGGING] = False
    except:
        print("Operating system not supported")

# Read the settings file and load into memory
get_options()

# Loop through the options menu until the user quits
run = True
while(run):
    print("===============")
    print_options()
    print("Please make a selection:")
    print("[1] Toggle Render Only")
    print("[2] Toggle Client Logging")
    print("[0] Exit")
    print("===============")

    user_response = input("Selection: ")
    user_response = int(user_response)

    if user_response is 1:
        # Toggle the render only setting
        print("Toggling render-only setting")
        options[RENDER_ONLY] = not options[RENDER_ONLY]
    elif user_response is 2:
        # Toggle the client logging setting
        print("Toggling client logging setting")
        options[CLIENT_LOGGING] = not options[CLIENT_LOGGING]
    elif user_response is 0:
        # Exit the loop and save the settings
        print("Saving and exiting...")
        save_options()
        run = False
    else:
        print("Invalid selection.")